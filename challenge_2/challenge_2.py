"""
Rhea Bonto
Applicant for IT Script Coder|Python Position
BCI Asia
"""

"""
Difficulty: medium

Needs to fix in reference to the sample output:
	1. Separate date and time, also fix format
	2. Separate meeting description and place
		*separated with <br/> or <span>
"""

from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import csv

url = 'https://businesspapers.parracity.nsw.gov.au/'

#Opens browser as well as the website indicated in the url
browser = webdriver.Firefox(executable_path = '/Users/Think/Downloads/geckodriver/geckodriver')
browser.get(url)

#Requests to get the contents of the website - get page source(html)
req = requests.get(url)
soup = BeautifulSoup(req.content, 'html.parser')
table = soup.find('table', {"class": 'bpsGridMenu'}) 					#finds the table with this particular class which has the desired content in the page


#Defines a function to get the headings of a table 
def get_table_head_fields(from_table):
	table_head_fields = []
	head = from_table.find('thead')										#finds thead which determines the heading of the table 					
	column = head.find_all('th')										#each th corresponds to the fields/columns in the table						
	for n in column: 
		n = n.getText().strip()											#getText and strip is used to get only the texts needed
		rem = '(HTML/PDF)'												#this text in the heading is removed to separate the html and the pdf					
		if rem in n:
			n = n.replace(rem,'-')
		table_head_fields.append(n)
	table_head_fields.insert(5, table_head_fields[2]+'Link to PDF')		#additional columns are inserted for the links to the pdf files  
	table_head_fields.insert(6, table_head_fields[3]+'Link to PDF')
	table_head_fields.insert(7, table_head_fields[4]+'Link to PDF') 
	table_head_fields[2] = table_head_fields[2]+'Link to HTML'			#existing columns are renamed as columns for the html files
	table_head_fields[3] = table_head_fields[3]+'Link to HTML'
	table_head_fields[4] = table_head_fields[4]+'Link to HTML'
	reorder = [0, 1, 2, 5, 3, 6, 4, 7]									#rearrangement is needed to follow the order of the original headings
	table_head_fields = [table_head_fields[i] for i in reorder]
	return table_head_fields

#Defines a function to get the entries in a table
def get_table_body_entries(from_table):
	table_body_entries = []
	body = from_table.find('tbody')											#finds tbody which determines the body of the table						
	rows = body.find_all('tr')												#finds tr which determines each entry/row in the body							
	for n in rows:
		entry = []
		row_fields = n.find_all('td')										#finds td which determines the content per field of an entry/row 					
		for k in row_fields:
			entry.append(k.getText().strip())								#getText and strip is used to get only the texts needed
		elem = ['Agenda HTML\nAgenda PDF', 'Agenda HTML', 'Agenda PDF', 	#the texts in this list appears consistent for each entry in a particular field as a hyperlink to the file for the field - unnecessary data(text)
				'Minutes HTML\nMinutes PDF', 'Minutes HTML', 'Minutes PDF']
		entry = ['' if i in elem else i for i in entry]						
		entry = entry[:2]													#are removed to be replaced by the their corresponding links instead

		links = n.find_all('a')												#finds a which contains the link 
		tags = ['grdMenu_hlAgendaHtm','grdMenu_hlAgendaPdf', 
				'grdMenu_hlMinutesHtm2', 'grdMenu_hlMinutesPdf2']			#elements in this list are substring of the id for each category/coulmn(4) of links - agenda_html, agenda_pdf, minutes_html, minutes_pdf
		hyperlinks = []														#based from the page source no row has contents for the attachments
		order = []
		for k in links:
			index = 0
			for i in tags:
				if i in k['id']:											#checks if the link is tagged for which column 
					redirect = url + k['href']								#adds the url to the href value to make it a valid link 
					hyperlinks.append(redirect)							
					order.append(index)
				index = index + 1
		for k in range(4):													#order corresponds to the categories agenda_html(0), agenda_pdf(1), minutes_html(2), minutes_pdf(3) as well as the position 
			if k not in order:												#this part completes the list by adding '' to fields with no links
				hyperlinks.insert(k, '')
		entry = entry + hyperlinks
		entry.insert(4, '')													#this field is added for the attachments_html which has no value
		entry.insert(5, '')													#this field is added for the attachments_pdf which has no value
		table_body_entries.append(entry)									#the entries/rows are accumulated to form the body				
	return table_body_entries

#Fetches the contents of the table
table_head = get_table_head_fields(table)									#simple fetch using the functions created
table_body = get_table_body_entries(table)
final_table = [table_head] + table_body 									#the headings and the body are combined to form the table

#Writes table in a csv file
with open('bonto_challenge_2_output.csv', 'w', newline='') as file:	
    writer = csv.writer(file)
    writer.writerows(final_table)

#Closes the browser
browser.close()
#Notifies that the file has been made
print('File is successfully written.')