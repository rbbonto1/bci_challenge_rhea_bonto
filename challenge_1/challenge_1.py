"""
Rhea Bonto
Applicant for IT Script Coder|Python Position
BCI Asia
"""

"""
Difficulty: easy

Needs to fix in reference to the sample output:
	1. include time for the first two columns as shown in the sample output
"""

from selenium import webdriver
from bs4 import BeautifulSoup
import requests
import csv

url = 'https://www.emservices.com.sg/tenders/'

#Opens browser as well as the website indicated in the url
browser = webdriver.Firefox(executable_path = '/Users/Think/Downloads/geckodriver/geckodriver')
browser.get(url)

#Requests to get the contents of the website - get page source(html)
req = requests.get(url)
soup = BeautifulSoup(req.content, 'html.parser')
table = soup.find('table') #finds table in the page

#Defines a function to get the headings of a table 
def get_table_head_fields(from_table):
	table_head_fields = []
	head = from_table.find('thead') 					#finds thead which determines the heading of the table
	column = head.find_all('th')						#each th corresponds to the fields/columns in the table
	for n in column:
		table_head_fields.append(n.getText().strip())	#getText and strip is used to get only the texts needed
	table_head_fields.insert(4, 'Link to Project') 		#another header is added beside Project Title to accomodate the link of the project
	return table_head_fields

#Defines a function to get the entries in a table 
def get_table_body_entries(from_table):
	table_body_entries = []
	body = from_table.find('tbody')						#finds tbody which determines the body of the table
	rows = body.find_all('tr')							#finds tr which determines each entry/row in the body
	for n in rows:
		entry = []
		row_fields = n.find_all('td') 					#finds td which determines the content per field of an entry/row
		for k in row_fields:
			entry.append(k.getText().strip())			#getText and strip is used to get only the texts needed
		entry.remove('Download')						#removes the text 'Download' which appears consistent for each entry in a particular field as a hyperlink to the file for the field - unecessary data(text)
		links = n.find_all('a')							#finds a which contains the link 	
		for k in links:
			entry.append(k['href'])	 					#appends the link to the file to the rest of the contents of the row
		reorder = [0, 1, 2, 3, 5, 4, 6] 				#since the texts and links were fetched separately, rearrangement is needed to follow the order of the headings 
		entry = [entry[i] for i in reorder]
		table_body_entries.append(entry)				#the entries/rows are accumulated to form the body
	return table_body_entries

#Fetches the contents of the table
table_head = get_table_head_fields(table)				#simple fetch using the functions created
table_body = get_table_body_entries(table)
final_table = [table_head] + table_body    				#the headings and the body are combined to form the table

#Writes table in a csv file
with open('challenge_1_output.csv', 'w', newline='') as file:	
    writer = csv.writer(file)
    writer.writerows(final_table)

#Closes the browser
browser.close()

#Notifies that the file has been made
print('File is successfully written.')
